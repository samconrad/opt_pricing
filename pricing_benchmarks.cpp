#include "benchmark/benchmark.h"
#include "Model.hpp"

#include <array>

using model::OptionStyle;

#define BENCHMARK_PRICING_SUITE(BenchmarkFunction)                             \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 2, OptionStyle::European);             \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 2, OptionStyle::American);             \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 15, OptionStyle::European);            \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 15, OptionStyle::American);            \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 50, OptionStyle::European);            \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 50, OptionStyle::American);            \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 100, OptionStyle::European);           \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 100, OptionStyle::American);           \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 1000, OptionStyle::European);          \
  BENCHMARK_TEMPLATE(BenchmarkFunction, 1000, OptionStyle::American);

constexpr double Underlying = 102.0;
constexpr double Strike = 100.0;
constexpr double Sigma = 0.5;
constexpr double TimeToExpiry = 0.25;
constexpr double RiskFreeRate = 0.1;

template <std::size_t NumSteps, model::OptionStyle Style>
static void BM_Price_Vector(benchmark::State &state) {
  while (state.KeepRunning()) {
    benchmark::DoNotOptimize(model::priceOption<
        NumSteps, model::OptionType::Call, Style, model::VectorWrapper>(
        Underlying, Strike, TimeToExpiry, RiskFreeRate, Sigma));
  }
}

template <std::size_t NumSteps, model::OptionStyle Style>
static void BM_Price_Array(benchmark::State &state) {
  while (state.KeepRunning()) {
    benchmark::DoNotOptimize(model::priceOption<
        NumSteps, model::OptionType::Call, Style, std::array>(
        Underlying, Strike, TimeToExpiry, RiskFreeRate, Sigma));
  }
}

BENCHMARK_PRICING_SUITE(BM_Price_Vector);
BENCHMARK_PRICING_SUITE(BM_Price_Array);

BENCHMARK_MAIN();
