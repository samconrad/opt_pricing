//  Copyright 2016 Sam Conrad
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

/// \mainpage Binomial options pricing library
///

/// \file Model.hpp
/// \brief C++ model for binomial options pricing.
///
/// \version 1.0
///
/// \author Sam Conrad
///
#ifndef MODEL_HPP
#define MODEL_HPP

#include <algorithm>
#include <array>
#include <cmath>
#include <type_traits>
#include <vector>

/// Computational model for binomial option pricing
namespace model {

enum class OptionType : std::int8_t { Put, Call };

enum class OptionStyle : std::int8_t { American, European };

/// A wrapper type for std::vector with templated size, to allow usage in place
/// of std::array
template <typename T, std::size_t S, typename... Args> struct VectorWrapper {
  constexpr VectorWrapper(Args &&... ExtraArgs)
      : Vec(S, std::forward<Args>(ExtraArgs)...) {}
  double &operator[](const std::size_t I) { return Vec[I]; }

private:
  std::vector<T, Args...> Vec;
};

/// \internal Auxiliary functions and types
namespace detail {

template <OptionType Type>
inline double intrinsicValue(double UnderlyingPrice, double StrikePrice) {
  if (Type == OptionType::Call) {
    return std::max(UnderlyingPrice - StrikePrice, 0.0);
  } else {
    return std::max(StrikePrice - UnderlyingPrice, 0.0);
  }
}

inline double binomialValue(double Discount, double QProb, double Up,
                            double Down) {
  return Discount * (QProb * Up + (1 - QProb) * Down);
}

constexpr std::size_t treeSize(std::size_t TreeDepth) {
  return TreeDepth * (TreeDepth + 1) / 2;
}

constexpr std::size_t storageSize(std::size_t TreeDepth, OptionStyle Style) {
  // American style reserves extra space after the regular nodes for exercise
  // payoffs not in the final row.
  return Style == OptionStyle::European ? treeSize(TreeDepth)
                                        : treeSize(TreeDepth) + TreeDepth - 1;
}

template <std::size_t TreeDepth, OptionStyle Style,
          template <typename, std::size_t, typename...> class C,
          typename... Args>
struct PriceTree {
  PriceTree(Args &&... ExtraArgs)
      : Container(std::forward<Args>(ExtraArgs)...) {}
  C<double, storageSize(TreeDepth, Style), Args...> Container;
  double &operator[](const std::size_t I) { return Container[I]; }
  constexpr std::size_t size() const { return storageSize(TreeDepth, Style); }
};

/// \internal Price the expiry payoffs, along with any additional exercise
/// payoffs needed for American-style pricing.  This works from the middle out
/// in both cases to avoid precision loss.
template <std::size_t TreeDepth, OptionStyle Style, OptionType Type,
          typename PriceTree>
inline void priceExpiryPayoffs(const double InitialUnderlyingPrice,
                               const double StrikePrice, const double Up,
                               const double Down, PriceTree &OutTree) {
  constexpr std::size_t TreeSize = treeSize(TreeDepth);
  constexpr std::size_t End = TreeSize - 1;
  constexpr std::size_t Start = TreeSize - TreeDepth;
  constexpr std::size_t Midpoint = (TreeDepth - 1) / 2;
  constexpr bool IsLevelEven = TreeDepth % 2 == 0;

  const double UpFactor = std::pow(Up, 2);
  const double DownFactor = std::pow(Down, 2);
  double PriceUp = InitialUnderlyingPrice * (IsLevelEven ? Up : 1);
  double PriceDown = InitialUnderlyingPrice * (IsLevelEven ? Down : 1);

  for (long i = Midpoint; i >= 0; i--) {
    OutTree[Start + i] = intrinsicValue<Type>(PriceUp, StrikePrice);
    PriceUp *= UpFactor;
    OutTree[End - i] = intrinsicValue<Type>(PriceDown, StrikePrice);
    PriceDown *= DownFactor;
  }

  // If this is an American-style option, price a secondary row with the
  // exercise prices that were skipped in the last row
  if (Style == OptionStyle::American) {
    constexpr std::size_t Tier2Start = TreeSize;
    constexpr std::size_t Tier2End = TreeSize + TreeDepth - 2;
    constexpr std::size_t Tier2Midpoint = (Tier2End - Tier2Start) / 2;
    PriceUp = InitialUnderlyingPrice * (IsLevelEven ? 1 : Up);
    PriceDown = InitialUnderlyingPrice * (IsLevelEven ? 1 : Down);
    for (long i = Tier2Midpoint; i >= 0; i--) {
      OutTree[Tier2Start + i] = intrinsicValue<Type>(PriceUp, StrikePrice);
      PriceUp *= UpFactor;
      OutTree[Tier2End - i] = intrinsicValue<Type>(PriceDown, StrikePrice);
      PriceDown *= DownFactor;
    }
  }
}

template <OptionStyle Value>
using StyleTag = std::integral_constant<OptionStyle, Value>;

/// \internal Finish pricing a European-style tree, assuming the expiry row has
/// been priced
template <std::size_t TreeDepth, typename PriceTree>
inline double finishTree(const double Discount, const double QProb,
                         PriceTree &Tree, StyleTag<OptionStyle::European>) {
  std::size_t Base = Tree.size() - TreeDepth - 1;
  for (std::size_t Slice = TreeDepth - 1; Slice > 0; Slice--) {
    for (std::size_t Ordinal = 0; Ordinal < Slice; Ordinal++) {
      const double Down = Tree[Base + Slice + 1];
      const double Up = Tree[Base + Slice];
      Tree[Base--] = binomialValue(Discount, QProb, Up, Down);
    }
  }
  return Tree[0];
}

/// \internal Finish pricing an American-style tree, assuming the expiry row and
/// additional exercise payoffs have been priced
template <std::size_t TreeDepth, typename PriceTree>
inline double finishTree(const double Discount, const double QProb,
                         PriceTree &Tree, StyleTag<OptionStyle::American>) {
  constexpr std::size_t TreeSize = treeSize(TreeDepth);
  constexpr std::size_t LevelOffsetDelta = TreeDepth - 1;

  std::size_t ExerciseOffset = TreeSize - 1;
  std::size_t Base = ExerciseOffset - TreeDepth;

  for (std::size_t Slice = TreeDepth - 1; Slice > 0; Slice--) {
    const int LevelSwitch = (Slice ^ TreeDepth) & 1;
    const std::size_t LevelOffset =
        ExerciseOffset + LevelSwitch * LevelOffsetDelta;
    for (std::size_t Ordinal = 0; Ordinal < Slice; Ordinal++) {
      const double Down = Tree[Base + Slice + 1];
      const double Up = Tree[Base + Slice];
      const double BinomialValue = binomialValue(Discount, QProb, Up, Down);
      const double ExerciseValue = Tree[LevelOffset - Ordinal];
      Tree[Base--] = std::max(BinomialValue, ExerciseValue);
    }
    ExerciseOffset -= LevelSwitch;
  }
  return Tree[0];
}

} // namespace detail

/// Price an option using the Cox, Ross, and Rubinstein binomial
/// model.  The tree is computed using a flat representation;  The default
/// container type uses std::vector, but an alternate container type and
/// additional args can be supplied.
/// \tparam <TreeDepth> The number of of time slices in the tree (including the
/// beginning and ending slice- for a single step, there are two time slices)
/// \tparam <Type> Put or a Call
/// \tparam <Style> American or European exercise style
/// \tparam <Storage> Optional: A container type that satisfies the following
/// traits: a template typename parameter defining the value type, a template
/// size_t parameter defining the container size, and any additional number of
/// typename args that can be forwarded to the constructor
/// \tparam <...Args> Optional: Additional template args to be forwarded to the
/// container type
/// \param UnderlyingPrice The current price of the underlying asset
/// \param StrikePrice The option strike price
/// \param TimeToExpiry The time to expiration of the option, expressed in
/// terms of years (ie 30 days = 0.08219)
/// \param RiskFreeRate The interest rate, expressed as a decimal (ie 1% = 0.1)
/// \param Sigma The volatility, expressed as a decimal (ie 50% = 0.5)
/// \param ExtraArgs Optional: Additional arguments to be forwarded
/// to the container
/// \return The computed option price
template <std::size_t TreeDepth, OptionType Type, OptionStyle Style,
          template <typename, std::size_t, typename...>
          class Storage = VectorWrapper,
          typename... Args>
inline double priceOption(const double UnderlyingPrice,
                          const double StrikePrice, const double TimeToExpiry,
                          const double RiskFreeRate, const double Sigma,
                          Args &&... ExtraArgs) {
  const double DeltaT = TimeToExpiry / (TreeDepth - 1);
  const double Discount = std::exp(-RiskFreeRate * DeltaT);
  const double Up = std::exp(Sigma * std::sqrt(DeltaT));
  const double Down = 1 / Up;
  const double QProb = (std::exp(RiskFreeRate * DeltaT) - Down) / (Up - Down);

  using namespace detail;

  PriceTree<TreeDepth, Style, Storage, Args...> Tree(
      std::forward<Args>(ExtraArgs)...);

  priceExpiryPayoffs<TreeDepth, Style, Type>(UnderlyingPrice, StrikePrice, Up,
                                             Down, Tree);
  return finishTree<TreeDepth>(Discount, QProb, Tree,
                               detail::StyleTag<Style>());
}

} // namespace model

#endif // MODEL_HPP
