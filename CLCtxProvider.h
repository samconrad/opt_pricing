/*
 * =====================================================================================
 *
 *       Filename:  CLCtxProvider.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  09/08/2016 09:55:58 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef CL_CONTEXT_H
#define CL_CONTEXT_H

#include <CL/cl.h>

#include <memory>
#include <vector>

namespace model {

class CLCtxProvider {
public:
  using DevIdIter = cl_device_id *;
  virtual ~CLCtxProvider() {}
  virtual cl_platform_id getPlatformId() const = 0;
  virtual DevIdIter beginDevIds() const = 0;
  virtual DevIdIter endDevIds() const = 0;
  virtual const std::shared_ptr<cl_context> &getCLContext() const = 0;
};

} // namespace model

#endif // CL_CONTEXT_H
