/*
 * =====================================================================================
 *
 *       Filename:  CLErrorStream.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  10/16/2016 06:21:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#ifndef CL_ERROR_STREAM_H
#define CL_ERROR_STREAM_H

#include <CL/cl.h>

#include <string>
#include <vector>

namespace model {

class CLErrorStream {

  template <std::size_t BatchSize> friend class CLOptionPricer;

public:
  explicit operator bool() const { return !m_ReturnCodes.empty(); }

  const std::string getMessage() const { return m_Message; }

  const std::vector<int> &getReturnCodes() const { return m_ReturnCodes; }

private:
  CLErrorStream &operator<<(const cl_int ReturnCode) {
    if (ReturnCode != CL_SUCCESS) {
      m_ReturnCodes.push_back(static_cast<int>(ReturnCode));
    }
    return *this;
  }

  CLErrorStream &operator<<(const std::string &Message) {
    m_Message += Message;
    return *this;
  }

  std::vector<int> m_ReturnCodes;
  std::string m_Message;
};
} // namespace model

#endif // CL_ERROR_STREAM_H
