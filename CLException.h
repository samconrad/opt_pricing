/*
 * =====================================================================================
 *
 *       Filename:  CLException.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  09/14/2016 09:24:51 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#ifndef CL_EXCEPTION_H
#define CL_EXCEPTION_H

#include <CL/cl.h>

#include <string>

namespace model {

class CLException : public std::runtime_error {
public:
  CLException(const std::string &message, const cl_int ErrorCode)
      : std::runtime_error(message), m_ErrorCode(ErrorCode) {}

  int getErrorCode() const { return static_cast<int>(m_ErrorCode); }

private:
  cl_int m_ErrorCode;
};

} // namespace model

#endif // CL_EXCEPTION_H
