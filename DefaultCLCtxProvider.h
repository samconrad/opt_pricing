/*
 * =====================================================================================
 *
 *       Filename:  CLCtxProvider.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  09/06/2016 08:26:20 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */
#ifndef DEFAULT_CL_CONTEXT_H
#define DEFAULT_CL_CONTEXT_H

#include "CLCtxProvider.h"
#include "CLException.h"
#include <CL/cl.h>
#include <functional>
#include <memory>
#include <vector>

namespace model {

class DefaultCLCtxProvider : public CLCtxProvider {

public:
  DefaultCLCtxProvider()
      : PlatformId(initPlatformId()), NumDeviceIds(initNumDeviceIds()),
        DeviceIds(initDeviceIds()),
        Ctx(new cl_context(initContext()), &deleteCLContext) {}

  cl_platform_id getPlatformId() const override { return PlatformId; }

  DevIdIter beginDevIds() const override { return DeviceIds.get(); }

  DevIdIter endDevIds() const override {
    return DeviceIds.get() + NumDeviceIds;
  }

  const std::shared_ptr<cl_context> &getCLContext() const override {
    return Ctx;
  }

private:
  static void deleteCLContext(cl_context *Ctx) {
    if (*Ctx != nullptr) {
      ::clReleaseContext(*Ctx);
    }
    delete Ctx;
  }

  cl_platform_id initPlatformId() const {
    cl_uint NumPlatforms = 0;
    cl_platform_id Id;
    cl_int ReturnCode = ::clGetPlatformIDs(1, &Id, &NumPlatforms);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error getting cl_platform_ids", ReturnCode);
    } else if (NumPlatforms < 1) {
      throw CLException("No platforms found", 0);
    }
    return Id;
  }

  std::size_t initNumDeviceIds() const {
    cl_uint NumDevices = 0;
    cl_int ReturnCode = ::clGetDeviceIDs(PlatformId, CL_DEVICE_TYPE_ALL, 0,
                                         nullptr, &NumDevices);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error getting cl_device_id count", ReturnCode);
    } else if (NumDevices < 1) {
      throw CLException("No devices found", 0);
    }
    return static_cast<std::size_t>(NumDevices);
  }

  std::unique_ptr<cl_device_id[]> initDeviceIds() const {
    std::unique_ptr<cl_device_id[]> DeviceIds =
        std::make_unique<cl_device_id[]>(NumDeviceIds);
    cl_int ReturnCode = ::clGetDeviceIDs(
        PlatformId, CL_DEVICE_TYPE_ALL, NumDeviceIds, DeviceIds.get(), nullptr);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error populating cl_device_ids", ReturnCode);
    }
    return DeviceIds;
  }

  cl_context initContext() {
    const cl_context_properties ContextProperties[] = {
        CL_CONTEXT_PLATFORM,
        reinterpret_cast<cl_context_properties>(PlatformId), 0, 0};

    cl_int ReturnCode = CL_SUCCESS;
    // TODO: investigate the callbacks here
    cl_context Ctx =
        clCreateContext(ContextProperties, NumDeviceIds, DeviceIds.get(),
                        nullptr, nullptr, &ReturnCode);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error creating cl_context", ReturnCode);
    }
    return Ctx;
  }

  cl_platform_id PlatformId = nullptr;
  const std::size_t NumDeviceIds;
  std::unique_ptr<cl_device_id[]> DeviceIds;
  std::shared_ptr<cl_context> Ctx;
};

} // namespace model
#endif // DEFAULT_CL_CONTEXT_H
