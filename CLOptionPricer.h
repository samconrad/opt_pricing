/*
 * =====================================================================================
 *
 *       Filename:  CLOptionPricer.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  09/08/2016 10:54:59 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef CL_OPTION_PRICER_H
#define CL_OPTION_PRICER_H

#include "CLCtxProvider.h"
#include "CLErrorStream.h"
#include "CLException.h"
#include "CLProgramSource.h"
#include "Model.hpp"
#include "Util.hpp"

#include <CL/cl.h>

#include <fstream>
#include <functional>
#include <type_traits>

// TODO:
// 1. split this out to a separate cpp file, batch does not need to be templated
// also, this will cause ODR violation because list of sources will be redefined when being included
// 2. finish splitting pricing kernels into different cl files
// 3. deal with kernel name mapping
// 4. TEST: make sure european pricing still works
// 5. reformat cl files
// 6. go over algo from paper for parallel american and dealing with dropping workers
// 7. TEST: make sure pricing expiries (american) works
namespace model {

template <std::size_t BatchSize> class CLOptionPricer {
public:
  using FloatBuff = std::array<double, BatchSize>;

  explicit CLOptionPricer(const CLCtxProvider &CP)
      : CommandQueue(initCommandQueue(CP), &::clReleaseCommandQueue),
        UnderlyingBuffer(initBuffer(*CP.getCLContext(), CL_MEM_READ_ONLY,
                                    BatchSize * sizeof(double)),
                         &::clReleaseMemObject),
        StrikeBuffer(initBuffer(*CP.getCLContext(), CL_MEM_READ_ONLY,
                                BatchSize * sizeof(double)),
                     &::clReleaseMemObject),
        VolBuffer(initBuffer(*CP.getCLContext(), CL_MEM_READ_ONLY,
                             BatchSize * sizeof(double)),
                  &::clReleaseMemObject),
        ExpiryBuffer(initBuffer(*CP.getCLContext(), CL_MEM_READ_ONLY,
                                BatchSize * sizeof(double)),
                     &::clReleaseMemObject),
        OutBuffer(initBuffer(*CP.getCLContext(), CL_MEM_WRITE_ONLY,
                             BatchSize * sizeof(double)),
                  &::clReleaseMemObject),
        Program(loadProgramFromFiles(CP, ProgramSources.begin(),
                                     ProgramSources.end()),
                &::clReleaseProgram),
        Kernel(loadKernel(Program, KernelName), &::clReleaseKernel) {}

  CLOptionPricer(const CLOptionPricer &) = delete;
  CLOptionPricer &operator=(const CLOptionPricer &) = delete;

  /*
   * TODO: test/debug against c++ lib
    clean up / unify api
   * perf tests
   */
  CLErrorStream priceBatch(const FloatBuff &UnderlyingPrices,
                           const FloatBuff &Strikes, const FloatBuff &Vols,
                           const FloatBuff &TimesToExpiry,
                           const double RiskFreeRate, const OptionType Type,
                           const std::size_t NumSteps, FloatBuff &BatchOut,
                           const std::size_t WorkersPerTree) noexcept {
    constexpr std::size_t BufferSize = BatchSize * sizeof(double);

    const cl_ulong CLTreeDepth = static_cast<cl_ulong>(NumSteps + 1);
    const cl_ulong CLTreeSize = (CLTreeDepth * (CLTreeDepth + 1)) / 2;
    const cl_double CLRate = static_cast<cl_double>(RiskFreeRate);
    const cl_int CLOptType = static_cast<cl_int>(Type);

    CLErrorStream ErrStream;

    // need to handle enqueueing multiple buffers for underlying/strikes/etc
    // maybe make this nonblocking and use the event args in an array to wait on
    // completion
    // maybe something to benchmark
    //
    // other option: CLProgram takes template param for tree size,
    // the generated string has something like $TREE_SIZE that can be regex
    // replaced
    // but try this first and then benchmark
    ErrStream
        << ::clEnqueueWriteBuffer(CommandQueue, UnderlyingBuffer, CL_TRUE, 0,
                                  BufferSize, &UnderlyingPrices[0], 0, nullptr,
                                  nullptr)
        << ::clEnqueueWriteBuffer(CommandQueue, StrikeBuffer, CL_TRUE, 0,
                                  BufferSize, &Strikes[0], 0, nullptr, nullptr)
        << ::clEnqueueWriteBuffer(CommandQueue, VolBuffer, CL_TRUE, 0,
                                  BufferSize, &Vols[0], 0, nullptr, nullptr)
        << ::clEnqueueWriteBuffer(CommandQueue, ExpiryBuffer, CL_TRUE, 0,
                                  BufferSize, &TimesToExpiry[0], 0, nullptr,
                                  nullptr)
        << ::clSetKernelArg(Kernel, 0, CLTreeSize * sizeof(double), nullptr)
        << ::clSetKernelArg(Kernel, 1, sizeof(cl_ulong), &CLTreeDepth)
        << ::clSetKernelArg(Kernel, 2, sizeof(cl_mem), UnderlyingBuffer.ptr())
        << ::clSetKernelArg(Kernel, 3, sizeof(cl_mem), StrikeBuffer.ptr())
        << ::clSetKernelArg(Kernel, 4, sizeof(cl_mem), VolBuffer.ptr())
        << ::clSetKernelArg(Kernel, 5, sizeof(cl_mem), ExpiryBuffer.ptr())
        << ::clSetKernelArg(Kernel, 6, sizeof(cl_double), &CLRate)
        << ::clSetKernelArg(Kernel, 7, sizeof(cl_int), &CLOptType)
        << ::clSetKernelArg(Kernel, 8, sizeof(cl_mem), OutBuffer.ptr());

    if (ErrStream) {
      return ErrStream << "Error(s) setting kernel args, no kernel enqueued";
    }

    // TODO: check max work group size (worker per tree configured in
    // constructor maybe)
    std::size_t LocalItemSize = WorkersPerTree;
    std::size_t GlobalItemSize = BatchSize * WorkersPerTree;
    ErrStream << ::clEnqueueNDRangeKernel(CommandQueue, Kernel, 1, nullptr,
                                          &GlobalItemSize, &LocalItemSize, 0,
                                          nullptr, nullptr);
    if (ErrStream) {
      return ErrStream << "Error preparing and enqueueing kernel";
    }

    ErrStream << ::clEnqueueReadBuffer(CommandQueue, OutBuffer, CL_TRUE, 0,
                                       BufferSize, &BatchOut[0], 0, nullptr,
                                       nullptr);
    if (ErrStream) {
      ErrStream << "Error reading kernel results";
    }

    return ErrStream;
  }

private:
  static constexpr char KernelName[]{"priceBatchEuropean"};
  static const std::array<std::string, 2> ProgramSources;

  cl_command_queue initCommandQueue(const CLCtxProvider &CtxProvider) {
    cl_command_queue CommandQueue = nullptr;
    if (CtxProvider.beginDevIds() == CtxProvider.endDevIds()) {
      throw CLException("No device ids present", 0);
    }
    cl_device_id DeviceId = *CtxProvider.beginDevIds();
    cl_int ReturnCode;
    CommandQueue = ::clCreateCommandQueue(*CtxProvider.getCLContext(), DeviceId,
                                          0, &ReturnCode);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error creating cl_command_queue", ReturnCode);
    }
    return CommandQueue;
  }

  cl_mem initBuffer(cl_context CLCtx, cl_mem_flags Flags,
                    const std::size_t BufferSize) {
    cl_int ReturnCode;
    cl_mem Buffer = nullptr;
    Buffer = ::clCreateBuffer(CLCtx, Flags, BufferSize, nullptr, &ReturnCode);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error creating cl_mem buffer", ReturnCode);
    }
    return Buffer;
  }

  cl_program loadProgramFromHeader(const CLCtxProvider &CtxProvider) {
    const char *SourceStrings[1];
    std::size_t SourceLengths[1];
    SourceStrings[0] = const_cast<const char *>(
        reinterpret_cast<char *>(clsource::pricing_cl));
    SourceLengths[0] = static_cast<std::size_t>(clsource::pricing_cl_len);

    return loadProgram(CtxProvider, 1, SourceStrings, SourceLengths);
  }

  template <typename It>
  cl_program loadProgramFromFiles(const CLCtxProvider &CtxProvider,
                                  It BeginSourceFiles, It EndSourceFiles) {
    // TODO: check sources aren't empty
    char *SourceStrings[std::distance(BeginSourceFiles, EndSourceFiles)];
    std::size_t SourceLengths[std::distance(BeginSourceFiles, EndSourceFiles)];
    std::size_t AllocatedSrcCount = 0;

    // Deletes any allocated source strings on return/exception
    util::ScopeExit<std::function<void(void)>> onExit(
        [&AllocatedSrcCount, SSP = &SourceStrings[0]]() {
          for (std::size_t i = 0; i < AllocatedSrcCount; i++) {
            delete[] SSP[i];
          }
        });

    for (auto SF = BeginSourceFiles; SF != EndSourceFiles; SF++) {
      std::ifstream Input(*SF, std::ios::in | std::ios::binary);
      if (!Input) {
        throw CLException("Could not open input file: " + *SF, 0);
      }
      Input.seekg(0, std::ios::end);
      const std::size_t Len = Input.tellg();
      SourceLengths[AllocatedSrcCount] = Len;
      SourceStrings[AllocatedSrcCount] = new char[Len];
      AllocatedSrcCount++;
      Input.seekg(0, std::ios::beg);
      Input.read(SourceStrings[AllocatedSrcCount - 1], Len);
      if (!Input) {
        throw CLException("Failed to read input file: " + *SF +
                              ", read only: " + std::to_string(Input.gcount()) +
                              " bytes",
                          0);
      }
    }

    return loadProgram(CtxProvider, AllocatedSrcCount,
                       const_cast<const char **>(SourceStrings), SourceLengths);
  }

  cl_program loadProgram(const CLCtxProvider &CtxProvider,
                         const std::size_t NumSources,
                         const char **SourceStrings,
                         std::size_t *SourceLengths) {
    cl_int ReturnCode;
    cl_program Program =
        ::clCreateProgramWithSource(*CtxProvider.getCLContext(), NumSources,
                                    SourceStrings, SourceLengths, &ReturnCode);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error loading cl_program", ReturnCode);
    }

    ReturnCode =
        ::clBuildProgram(Program, std::distance(CtxProvider.beginDevIds(),
                                                CtxProvider.endDevIds()),
                         CtxProvider.beginDevIds(), nullptr, nullptr, nullptr);
    if (ReturnCode != CL_SUCCESS) {
      std::size_t LogSize;
      ::clGetProgramBuildInfo(Program, *CtxProvider.beginDevIds(),
                              CL_PROGRAM_BUILD_LOG, 0, nullptr, &LogSize);
      char Log[LogSize];
      ::clGetProgramBuildInfo(Program, *CtxProvider.beginDevIds(),
                              CL_PROGRAM_BUILD_LOG, LogSize, Log, nullptr);
      ::clReleaseProgram(Program);
      throw CLException("Error building cl_program, build error log: \n" +
                            std::string(Log),
                        ReturnCode);
    }

    return Program;
  }

  cl_kernel loadKernel(cl_program Program, const char *KernelName) {
    cl_int ReturnCode;
    cl_kernel Kernel = ::clCreateKernel(Program, KernelName, &ReturnCode);
    if (ReturnCode != CL_SUCCESS) {
      throw CLException("Error creating cl_kernel", ReturnCode);
    }
    return Kernel;
  }

  util::CLResource<cl_command_queue> CommandQueue;
  util::CLResource<cl_mem> UnderlyingBuffer;
  util::CLResource<cl_mem> StrikeBuffer;
  util::CLResource<cl_mem> VolBuffer;
  util::CLResource<cl_mem> ExpiryBuffer;
  util::CLResource<cl_mem> OutBuffer;
  util::CLResource<cl_program> Program;
  util::CLResource<cl_kernel> Kernel;
};

template <std::size_t BatchSize>
const std::array<std::string, 2> CLOptionPricer<BatchSize>::ProgramSources{
    {"pricing_american.cl", "pricing_european.cl"}};

template <std::size_t BatchSize>
constexpr char CLOptionPricer<BatchSize>::KernelName[];

} // namespace model

#endif // CL_OPTION_PRICER_H
